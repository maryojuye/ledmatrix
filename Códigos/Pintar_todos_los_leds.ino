#include <FastLED.h>

#define LED_PIN     5
#define NUM_LEDS    64
#define BRIGHTNESS  64
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
#define UPDATES_PER_SECOND 100

char color=0;
char received;


void setup() {
    delay( 3000 ); // power-up safety delay
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
    Serial.begin(9600);
    
}
void loop()
{
  if(Serial.available()>0){
    color = Serial.read();
    char Rec = char(color);
    if (Rec != '0')
    {
      Serial.println(Rec);
    }
  }

if (color == 'r')//Rojo //Si la variable color es igual a r, pinta de color rojo
  {
        for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB(255, 0, 0); //Por cada número de LEDs de la tira, pintara de color rojo todos los LEDs
    }

  }
if (color == 'v')//Verde
  {
        for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB(0, 128, 0); //Por cada número de LEDs de la tira, pintara de color verde todos los LEDs
    }

  }
 if (color == 'a')//Azul
  {
        for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB(0, 0, 255); //Por cada número de LEDs de la tira, pintara de color azul todos los LEDs
    }

  }
    if (color == 'n')//naranja
  {
           for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB(255, 165, 0); //Por cada número de LEDs de la tira, pintara de color naranja todos los LEDs
    }

  }
if (color == 'l')//Lila
  {
        for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB(128, 0, 128); //Por cada número de LEDs de la tira, pintara de color lila todos los LEDs
    }

  }
if (color == 'y')//Amarillo
  {
        for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB(255, 255, 0); //Por cada número de LEDs de la tira, pintara de color amarillo todos los LEDs
    }
  }  
    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND);   
}

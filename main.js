var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[slideIndex-1].style.display = "block";
}


$(document).ready(function(){
  console.log("Welcome to jQuery.");

  includeHTML();

  $("#arduinoim").mouseover(function(){
    $("#elegoo").show();
    $("#arduinoim").css("filter", "grayscale(100%)");
  });
  $("#arduinoim").mouseout(function(){
    $("#elegoo").hide();
    $("#arduinoim").css("filter", "grayscale(0%)");
  });

  $("#fuente").mouseover(function(){
    $("#aliment").show();
    $("#fuente").css("filter", "grayscale(100%)");
  });
  $("#fuente").mouseout(function(){
    $("#aliment").hide();
    $("#fuente").css("filter", "grayscale(0%)");
  });

  $("#jumper").mouseover(function(){
    $("#cables").show();
    $("#jumper").css("filter", "grayscale(100%)");
  });
  $("#jumper").mouseout(function(){
    $("#cables").hide();
    $("#jumper").css("filter", "grayscale(0%)");
  });

  $("#modem").mouseover(function(){
    $("#bluetooth").show();
    $("#modem").css("filter", "grayscale(100%)");
  });
  $("#modem").mouseout(function(){
    $("#bluetooth").hide();
    $("#modem").css("filter", "grayscale(0%)");
  });

  $("#leds").mouseover(function(){
    $("#rgb").show();
    $("#leds").css("filter", "grayscale(100%)");
  });
  $("#leds").mouseout(function(){
    $("#rgb").hide();
    $("#leds").css("filter", "grayscale(0%)");
  });

  



});

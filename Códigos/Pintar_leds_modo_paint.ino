#include <FastLED.h>
  
#define LED_PIN     5
#define NUM_LEDS    64
#define BRIGHTNESS  90
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
#define UPDATES_PER_SECOND 100

const byte numChars = 32;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing

      // variables to hold the parsed data
char messageFromPC[numChars] = {0};
int integerFromPC = 0;
int integerred = 0;
int integerblue = 0;
int integergreen = 0;
float floatFromPC = 0.0;

boolean newData = false;

//============

void setup() {
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
    Serial.begin(9600);
    Serial.println();
}

//============

void loop() {
    recvWithStartEndMarkers();
    if (newData == true) {
        strcpy(tempChars, receivedChars);
            // esta copia temporal es necesaria para proteger los datos originales porque strtok() 
            //utilizado en parseData() reemplaza las comas por \0
        parseData();
        showParsedData();
        showColores();
        newData = false;
       bug();
      paintone();
      clearall();  
    }      
}

//============

void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}

//============

void parseData() {      // dividir los datos en sus partes

    char * strtokIndx; // esto es utilizado por strtok() como un índice

    strtokIndx = strtok(tempChars,",");      // obtener la primera parte - la cadena
    strcpy(messageFromPC, strtokIndx); // copiarlo a messageFromPC
 
    strtokIndx = strtok(NULL, ","); // esto continúa donde la llamada anterior dejó
    integerFromPC = atoi(strtokIndx); // convertir esta parte en un int

    strtokIndx = strtok(NULL, ",");
    floatFromPC = atof(strtokIndx);     // convertir esta parte en un flot (para poder tener números decimales)
    
     strtokIndx = strtok(NULL, ","); 
    integerred = atoi(strtokIndx); 

    strtokIndx = strtok(NULL, ",");
    integergreen = atoi(strtokIndx);
    
    strtokIndx = strtok(NULL, ","); 
    integerblue = atoi(strtokIndx);     
}

//============
void showParsedData() { 
    Serial.print("Message ");
    Serial.println(messageFromPC);//Muestra si hay algun texto
    Serial.print("Integer ");
    Serial.println(integerFromPC);//Muestra los valores númericos
    Serial.print("Float ");
    Serial.println(floatFromPC);//Muestra los decimales
}
void showColores(){
    Serial.print("R ");
    Serial.println(integerred);//Muestra el valor del color rojo
    Serial.print("G ");
    Serial.println(integergreen);//Muestra el valor del color verde
    Serial.print("B ");
    Serial.println(integerblue);//Muestra el valor del azul
  
  }

void bug(){ //Este void esta porque sin el no podria pintar los leds de uno en uno.
  if (floatFromPC = 2.0){
       leds[0] = CRGB(integerred, integergreen, integerblue); 
    }
FastLED.show();
}
  
void paintone(){ //Sirve para escoger el led a pintar y luego los números de los colores creados.

  if (integerFromPC>0 && integerFromPC<65){
    leds[integerFromPC] = CRGB(integerred, integergreen, integerblue);
    }
    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND);
  }
 void clearall(){ //Para esborrar todo lo pintado
  if (floatFromPC = 3.0){
       for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB(0, 0, 0); 
    }
  }
 }

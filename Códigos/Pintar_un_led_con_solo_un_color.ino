#include <FastLED.h>
  
#define LED_PIN     5
#define NUM_LEDS    64
#define BRIGHTNESS  64
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
#define UPDATES_PER_SECOND 100

void setup() {
    delay( 3000 ); // power-up safety delay
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
    Serial.begin(9600);
} 

void loop()
{
  if(Serial.available()>0){
    delay(20);
   String bufferString = "";
   
   bufferString += (char)Serial.read();

    Serial.println(bufferString);
     int convert = String(bufferString).toInt();
     
     if (convert>=0 && convert<65){
       leds[convert] = CRGB::Red;
      }
  }
}
